/*
  WiFi relay with web interface
  and over the air firmware update
  To upload through terminal you can use: curl -F "image=@firmware.bin" esp8266-webupdate.local/update
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "bootstrap-4.3.1.css.gz.h"

static const char* host_prefix = "esp8266_";
static const char* ssid = "wifissid";
static const char* password = "wifipassword";

static const char* http_username = "admin";
static const char* http_password = "password";

ESP8266WebServer server(80);

static char* hostname;

//------------------
//Inputs and outputs
static const uint8_t GPIO_LED = 2;
static void led_on() {
  digitalWrite(GPIO_LED, LOW);
  pinMode(GPIO_LED, OUTPUT);
}
static void led_off() {
  pinMode(GPIO_LED, INPUT_PULLUP);
}
static void led_init() {
  led_off();
}
//-------------------
static enum {RELAY_OFF = 0, RELAY_ON = 1} relay_state;
static const uint8_t GPIO_RELAY = 0;
static void relay_on() {
  relay_state = RELAY_ON;
  digitalWrite(GPIO_RELAY, LOW);
  pinMode(GPIO_RELAY, OUTPUT);
}
static void relay_off() {
  relay_state = RELAY_OFF;
  pinMode(GPIO_RELAY, INPUT_PULLUP);
}
static void relay_init() {
  relay_off();
}
//-------------------
static const uint8_t GPIO_BUTTON = 3;
static uint32_t button() {
  if (digitalRead(GPIO_BUTTON)) {
    return 0;
  }
  return 1;
}
static void button_init() {
  pinMode(GPIO_BUTTON, INPUT_PULLUP);
}
//-------------------

void sendIndexPage() {
    //if (!server.authenticate(http_username, http_password)) {
    //  return server.requestAuthentication();
    //}
      String page =
"<!doctype html><html lang=\"cs\"><head><meta charset=\"utf-8\"/>"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">"
"<link rel=\"stylesheet\" href=\"/bootstrap.css\">"
"<title>WiFi Relay ";
      page += hostname;
      page += "</title></head><body>"
"<div class=\"container-fluid\">"
  "<div class=\"row\">"
    "<div class=\"col-md-12\">"
      "<div class=\"page-header\">"
        "<h1>Sauna</h1>"
      "</div>"
      "<div class=\"card\">"
        "<h1 class=\"card-header\">"
          "<span style='";
      page += (relay_state == RELAY_OFF) ? "display:none" : "";
      page += "' id='idstate1' class='badge badge-pill badge-success'>zapnuto</span>"
          "<span style='";
      page += (relay_state == RELAY_ON) ? "display:none" : "";
      page += "' id='idstate0' class='badge badge-pill badge-danger'>vypnuto</span>"
          "</span>"
        "</h1>"
        "<div class=\"card-body\">"
          "<form method='post' class='card-text'>"
            "<button type='submit' formaction='/1' class='btn btn-outline-success btn-lg mr-5'>zapnout</button>"
            "<button type='submit' formaction='/0' class='btn btn-outline-danger btn-lg'>vypnout</button>"
          "</form>"
        "</div>"
      "</div>"
      "<dl class=\"card-footer mt-3\">"
        "<dt>IP address</dt>"
        "<dd>";
      page += WiFi.localIP().toString();
      page +=
        "</dd>"
        "<dt>hostname</dt>"
        "<dd>";
      page += hostname;
      page +=
        "</dd>"
      "</dl>"
    "</div>"
  "</div>"
"</div>"
"<script type='text/javascript'>"
"var state0_el = document.getElementById('idstate0');"
"var state1_el = document.getElementById('idstate1');"
"var state = 1;"
"function scheduleCheck() {"
  "window.setTimeout(checkState, 5*1000);"
"}"
"function elShow(el) {"
  "el.style.display='inline-block';"
"}"
"function elHide(el) {"
  "el.style.display='none';"
"}"
"function checkState() {"
  "fetch('/state').then(function(response) {"
    "return response.json();"
  "}).then(function(j) {"
    "state = j.state;"
    "if (state===1) {"
      "elHide(state0_el);"
      "elShow(state1_el);"
    "} else {"
      "elShow(state0_el);"
      "elHide(state1_el);"
    "}"
    "scheduleCheck();"
  "}).catch(function() {"
    "scheduleCheck();"
  "});"
"}"
"scheduleCheck();"
"</script>";
      page += "</body></html>";

      server.send(200, "text/html", page);
}

void setup(void) {
  relay_init();
  button_init();
  led_init();
  hostname = (char*)malloc(strlen(host_prefix) + 10);
  sprintf(hostname, "%s%x", host_prefix, ESP.getChipId());
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //Serial.println("WiFi Connect Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  MDNS.begin(hostname);
  server.on("/", HTTP_GET, sendIndexPage);
  server.on("/bootstrap.css", HTTP_GET, []() {
    server.sendHeader("Content-Encoding", "gzip");
    server.send_P(200, PSTR("text/css"), (const char*)bootstrap_4_3_1_css_gz, bootstrap_4_3_1_css_gz_len);
  });
  server.on("/fw/", HTTP_GET, []() {
    if (!server.authenticate(http_username, http_password)) {
      return server.requestAuthentication();
    }
    server.send_P(200, PSTR("text/html; charset=utf-8"), PSTR("<!DOCTYPE html><html><head><meta charset=\"utf-8\"/><title>FW update</title></head><body><form method='POST' action='/update' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Update'></form></body></html>"));
  });
  server.on("/update", HTTP_POST, []() {
    if (!server.authenticate(http_username, http_password)) {
      return server.requestAuthentication();
    }
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      led_on();
      WiFiUDP::stopAll();
      uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
      if (!Update.begin(maxSketchSpace)) { //start with max available size
        //Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        //Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        //Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        //Update.printError(Serial);
      }
      //Serial.setDebugOutput(false);
      led_off();
    }
    yield();
  });
  server.on("/1", HTTP_ANY, []() {
    //if (!server.authenticate(http_username, http_password)) {
    //  return server.requestAuthentication();
    //}
    relay_on();
    server.sendHeader("Location", "/");
    server.send(303);
  });
  server.on("/0", HTTP_ANY, []() {
    //if (!server.authenticate(http_username, http_password)) {
    //  return server.requestAuthentication();
    //}
    relay_off();
    server.sendHeader("Location", "/");
    server.send(303);
  });
  server.on("/state", HTTP_GET, []() {
    String resp = "{\"state\":";
    resp += (relay_state == RELAY_ON) ? "1" : "0";
    resp += "}";
    server.send(200, "application/json", resp);
  });
  server.begin();
  MDNS.addService("http", "tcp", 80);
}

void loop(void) {
  yield();
  server.handleClient();
  MDNS.update();
}
